const countWord = (word, words) => {
    let oneWord = word.toLowerCase();
    let manyWords = words.toLowerCase();
    let hasil = manyWords.split(oneWord);

    return hasil.length-1;
};

const wordToCount = "cat";
const input = "cat and dog. cat and sheep";

console.log(countWord(wordToCount, input));
