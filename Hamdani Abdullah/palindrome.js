const palindrome = (input) => {
    let reverseWord = "";
    let output;
    
    for(let i = input.length - 1; i >= 0; i--) {
        reverseWord += input[i];
    }
    if(input === reverseWord){
        output = "true";
    } 
    else {
        output = "false";
    }
    return output
};
const input = "ada";

console.log(palindrome(input));

/*cara kedua 
const palindrome = (word) => {
    let splitWord = word.split("");
    let reverseWord = splitWord.reverse();
    let outputWord = reverseWord.join("");

    if (outputWord === word) {
      return "True"
    }
    else {
      return "False";
    }
};
const input = "ada";

console.log(palindrome(input));
*/