const fizzBuzz = (number) => {
    let newNumber = "";
    
    for (let b = 1; b <= number; b++ ) {
        if (b % 3 === 0 && b % 5 ===0) {
            newNumber += "FizzBuzz ";
        } 
        else if (b % 3 === 0) {
            newNumber += "Fizz ";
        } 
        else if (b % 5 === 0) {
            newNumber += "Buzz ";
        } 
        else {
            newNumber += `${b} `;
        }
    }
    return newNumber ;
};

const input = 16;

console.log(fizzBuzz(input));
