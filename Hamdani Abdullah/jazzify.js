const jazzify = (chord) => {

    for (let i=0; i < chord.length; i++){
        if(chord[i][chord[i].length -1] !== "7"){
            chord[i] +="7";
        }
    }
    return chord;
};

const input = ["G", "Am", "C"];

console.log(jazzify(input));

