const findMaxNumber = (arr) => {
    let maxNumber = arr[0];
    for (let i = 0; i < arr.length; i++) {
        if ( arr[i] > maxNumber) {
             maxNumber = arr[i];
        }
    }
    return maxNumber;
};
const input = [8, 2, 5, 10, 18, 9, 15];

console.log(findMaxNumber(input));

