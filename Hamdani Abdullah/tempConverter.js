const tempConverter = (input) => {
  let numb = input.match(/\d/g);
  let newNumb = numb.join("");
  let output = " ";

  if (!isNaN(input)) {
    output = "Error Boy";
  }
   else if (input[input.length-1] === "C") {
     output = `"${parseInt((newNumb* 9 / 5) + 32)}F"`;
   }
   else if (input[input.length-1] === "F") {
    output = `"${parseInt((newNumb-32) * 5 / 9)}C"`;
   }
   return output;
};

const input = "35C";

console.log(tempConverter(input));