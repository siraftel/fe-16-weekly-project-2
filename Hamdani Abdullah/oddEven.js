const oddEven = (input) => {
    if (input % 2 === 0) {
        return input + ' is Even Number';
    }
    else {
        return input + ' is Odd Number';
    }
}
const input = 4;

console.log(oddEven(input));
