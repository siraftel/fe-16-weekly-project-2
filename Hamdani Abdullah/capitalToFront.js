const capitalToFront = (kata) => {
    let upperLetter = "";
    let lowerLetter = "";

    for(let i = 0; i < kata.length; i++) {
        if(kata[i] === kata[i].toUpperCase()) {
            upperLetter += kata[i];
        } 
        else {
            lowerLetter += kata[i];
        }
    }
    return upperLetter + lowerLetter;
};

const input = "hEllO";

console.log(capitalToFront(input));
